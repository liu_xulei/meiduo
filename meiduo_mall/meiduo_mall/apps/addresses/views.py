from django.contrib.auth.decorators import login_required
from django.shortcuts import render
from django.views import View
from django.utils.decorators import method_decorator
from django.core.cache import cache
# Create your views here.

from addresses.models import Address, Area
from django.http import JsonResponse
import json
import re


@method_decorator(login_required, name='dispatch')
class AssressesView(View):
    def get(self, request):
        user = request.user
        addresses = Address.objects.filter(user=user, is_deleted=False)
        addresses_list = []
        for address in addresses:
            addresses_list.append({
                'id': address.id,
                'receiver': address.receiver,
                'province': address.province.name,
                'city': address.city.name,
                'district': address.district.name,
                'place': address.place,
                'mobile': address.mobile,
                'tel': address.tel,
                'email': address.email,
                'title': address.title
            })

        return render(request, 'user_center_site.html', {'addresses': addresses_list})


class AreasView(View):
    def get(self, request):
        area_id = request.GET.get('area_id')
        if area_id is None:
            province_list = cache.get('province_list')
            if province_list is None:
                data = Area.objects.filter(parent_id=None)
                province_list = []
                for province in data:
                    province_list.append({'id': province.id, 'name': province.name})
                cache.set('province_list',province_list,60*60)


        else:
            province_list= cache.get('province_list_%s'%area_id)
            if province_list is None:
                data = Area.objects.filter(parent_id=area_id)
                province_list = []
                for province in data:
                    province_list.append({'id': province.id, 'name': province.name})
                cache.set('province_list_%s'%area_id,province_list,60*60)
        return JsonResponse({'code': 0, 'province_list': province_list})


class AddressesCreateView(View):
    def post(self, request):

        data = request.body.decode()
        data_dict = json.loads(data)
        province_id = data_dict.get('province_id')
        city_id = data_dict.get('city_id')
        district_id = data_dict.get('district_id')
        place = data_dict.get('place')
        mobile = data_dict.get('mobile')
        tel = data_dict.get('tel')
        title = data_dict.get('title')
        receiver = data_dict.get('receiver')

        if len(receiver) < 2 or len(receiver) > 10:
            return JsonResponse({'code': 5001})
        if not re.match(r'1[3-9]\d{9}$', mobile):
            return JsonResponse({'code': 4007})

        user = request.user

        addresses = Address.objects.create(user=user, mobile=mobile, province_id=province_id, city_id=city_id,
                                           district_id=district_id, place=place, tel=tel, title=title,
                                           receiver=receiver)

        addresses_dict = {'id': addresses.id, 'receiver': addresses.receiver, 'province': addresses.province.name,
                          'city': addresses.city.name, 'district': addresses.district.name, 'mobile': addresses.mobile,
                          'tel': addresses.tel, 'title': addresses.title, 'place': addresses.place}

        return JsonResponse({'code': 0, 'address': addresses_dict})


class AddressCreateView(View):
    def put(self, request, pk):
        data = request.body.decode()
        data_dict = json.loads(data)
        province_id = data_dict.get('province_id')
        city_id = data_dict.get('city_id')
        district_id = data_dict.get('district_id')
        place = data_dict.get('place')
        mobile = data_dict.get('mobile')
        tel = data_dict.get('tel')
        title = data_dict.get('title')
        receiver = data_dict.get('receiver')

        if len(receiver) < 2 or len(receiver) > 10:
            return JsonResponse({'code': 5001})
        if not re.match(r'1[3-9]\d{9}$', mobile):
            return JsonResponse({'code': 4007})

        addresses = Address.objects.get(id=pk)
        addresses.mobile = mobile
        addresses.province_id = province_id
        addresses.city_id = city_id
        addresses.district_id = district_id
        addresses.place = place
        addresses.tel = tel
        addresses.title = title
        addresses.receiver = receiver

        addresses_dict = {'id': addresses.id, 'receiver': addresses.receiver, 'province': addresses.province.name,
                          'city': addresses.city.name, 'district': addresses.district.name, 'mobile': addresses.mobile,
                          'tel': addresses.tel, 'title': addresses.title, 'place': addresses.place}
        addresses.save()
        return JsonResponse({'code': 0, 'address': addresses_dict})

    def delete(self,request,pk):
        try:
            address = Address.objects.get(id=pk)
        except:
            return JsonResponse({"code": 5001})
        address.is_deleted = True
        address.save()
        return JsonResponse({'code': '0'})


class AddressDefaultView(View):
    def put(self,request,pk):
        try:
            address = Address.objects.get(id = pk)
        except:
            return JsonResponse({"code": 5001})
        user = request.user
        user.default_address_id = address
        user.save()
        return JsonResponse({'code': '0'})


class AddressTitleView(View):
    def put(self,request,pk):
        try:
            address = Address.objects.get(id=pk)
        except:
            return JsonResponse({"code": 5001})

        title = json.loads(request.body.decode()).get('title')
        address.title = title
        address.save()
        return JsonResponse({'code': '0'})



