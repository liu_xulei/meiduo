from django.conf.urls import url
from django.contrib import admin
from . import views
urlpatterns = [

    url(r'^addresses/$', views.AssressesView.as_view()),
    url(r'^areas/$', views.AreasView.as_view()),
    url(r'^addresses/create/$', views.AddressesCreateView.as_view()),
    url(r'^addresses/(?P<pk>\d+)/$', views.AddressCreateView.as_view()),
    url(r'^addresses/(?P<pk>\d+)/default/$', views.AddressDefaultView.as_view()),
    url(r'^addresses/(?P<pk>\d+)/title/$', views.AddressTitleView.as_view()),

]
