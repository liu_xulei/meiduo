from django.conf.urls import url
from django.contrib import admin
from . import views
urlpatterns = [
    url(r'^list/(?P<categorie_id>\d+)/(?P<page_num>\d+)/',views.GoodListView.as_view()),
    # 获取热销商品
    url(r'hot/(?P<categorie_id>\d+)/$', views.GoodsHotView.as_view()),
    # 商品详情页展示
    url(r'detail/(?P<pk>\d+)/$', views.GoodsDetailView.as_view()),
    # 商品分类访问量记录
    url(r'detail/visit/(?P<pk>\d+)/$', views.GoodsVisitView.as_view()),
    url(r'browse_histories/$', views.GoodsHistoriesView.as_view()),
    # 获取详情页商品评论信息
    url(r'comment/(?P<pk>\d+)/$', views.GoodsCommentView.as_view()),

]
