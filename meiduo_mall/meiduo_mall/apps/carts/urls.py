from django.conf.urls import url
from django.contrib import admin
from . import views
urlpatterns = [
    # 购物车
    url(r'^carts/$',views.CartsView.as_view()),
    # 全选
    url(r'^carts/selection/$',views.CartsSelectView.as_view()),
    # 购物车的简单展示
    url(r'^carts/simple/$', views.CartsSimpleView.as_view()),
]

