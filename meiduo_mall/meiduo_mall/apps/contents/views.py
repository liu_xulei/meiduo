from django.shortcuts import render,redirect
from django.views import View
from goods.models import SKU
from collections import OrderedDict
from goods.models import GoodsChannel
from contents.models import ContentCategory
from contents.utils import get_categories


# Create your views here.
class ImageView(View):
    def get(self, request):
        skus = SKU.objects.all()
        return render(request, 'img.html', {'skus': skus})


class IndexView(View):
    def get(self, request):
        # 1.渲染分类导航数据
        categories = get_categories()

        # 2.渲染广告数据
        contents = {}
        # 查询所有广告分类
        contentcategorys = ContentCategory.objects.all()
        for contentcategory in contentcategorys:
            contents[contentcategory.key] = contentcategory.content_set.filter(status=True).order_by('sequence')

        data = {
            'categories': categories,
            'contents': contents,
        }
        print(data)
        return render(request, 'index.html',data)
