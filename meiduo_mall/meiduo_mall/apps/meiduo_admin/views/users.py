from rest_framework.generics import ListAPIView,ListCreateAPIView
from rest_framework.permissions import IsAdminUser
from users.models import User
from meiduo_admin.serializers.users import UsersSerializer
from meiduo_admin.utils import PageNumberPagination, PageNum


class Users(ListCreateAPIView):
    # 指定序列化器
    serializer_class = UsersSerializer
    # 查询集
    queryset = User.objects.filter(is_staff=False)
    # 指定分页器
    pagination_class = PageNum
    # 认证
    permission_classes = [IsAdminUser]


    def get_queryset(self):
        keyword = self.request.query_params.get('keyword')
        if keyword is None or keyword == '':
            return User.objects.filter(is_staff=False)
        else:
            return User.objects.filter(is_staff=False,username__contains=keyword)




