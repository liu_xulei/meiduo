from rest_framework import serializers
from rest_framework.permissions import IsAdminUser
from rest_framework.response import Response
from rest_framework.views import APIView
from order.models import OrderInfo
from users.models import User
from datetime import date, timedelta
from goods.models import GoodsVisitCount



# 序列化器
class GoodsSerializer(serializers.ModelSerializer):
    # 指定返回分类名称
    category=serializers.StringRelatedField(read_only=True)
    class Meta:
        model=GoodsVisitCount
        fields=('count','category')
class UserTotalCountView(APIView):
    # 获取用户总量
    permission_classes = [IsAdminUser]

    def get(self, requset):
        count = User.objects.filter(is_staff=False).count()

        return Response({
            'count': count,
            'data': date.today()
        })


class UserDayCountView(APIView):
    # 日新增用户
    permission_classes = [IsAdminUser]

    def get(self, requset):
        now_date = date.today()

        count = User.objects.filter(date_joined__gte=now_date, is_staff=False).count()

        return Response({
            'count': count,
            'data': date.today()
        })


class UserDayActiveCountView(APIView):
    # 日活跃总量
    permission_classes = [IsAdminUser]

    def get(self, requset):
        now_date = date.today()
        # print(now_date)
        count = User.objects.filter(last_login__gte=now_date, is_staff=False).count()

        return Response({
            'count': count,
            'data': date.today()
        })


class UserDayOrdersView(APIView):
    # 日下单总量
    permission_classes = [IsAdminUser]

    def get(self, requset):
        now_date = date.today()

        count = User.objects.filter(orderinfo__create_time=now_date, is_staff=False).count()

        return Response({
            'count': count,
            'data': date.today()
        })


class UserMonthIncrementView(APIView):
    permission_classes = [IsAdminUser]

    def get(self, request):
        now_date = date.today()
        old_date = now_date - timedelta(days=29)
        date_list = []
        for i in range(30):
            index_date = old_date + timedelta(days=i)
            next_date = old_date + timedelta(days=i + 1)

            count = User.objects.filter(date_joined__gte=index_date, date_joined__lt=next_date, is_staff=False).count()

            date_list.append({
                'count': count,
                'date': index_date
            })

        return Response(date_list)


class UserGoodsDayView(APIView):
    permission_classes = [IsAdminUser]

    def get(self, request):
        now_date = date.today()
        # goods = GoodsVisitCount.objects.filter(date=now_date)
        # data_list = []
        # for good in goods:
        #     data_list.append({
        #         'count': good.count,
        #         'category': good.category.name
        #     })
        # return Response(data_list)
        data=GoodsVisitCount.objects.filter(date=now_date)
                # 序列化返回分类数量
        ser=GoodsSerializer(data,many=True)

        return Response(ser.data)