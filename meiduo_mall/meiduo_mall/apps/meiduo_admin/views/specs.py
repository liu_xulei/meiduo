from rest_framework.permissions import IsAdminUser
from rest_framework.viewsets import ModelViewSet
from rest_framework.response import Response
from meiduo_admin.serializers.specs import SPUSpecificationSerializer,SPUSerializer
from goods.models import SPUSpecification, SPU
from meiduo_admin.utils import PageNum
class Specsview(ModelViewSet):
    # 指定序列化器
    serializer_class = SPUSpecificationSerializer
    # 指定查询集
    queryset = SPUSpecification.objects.all()
    # 分页
    pagination_class = PageNum
    # 认证
    permission_classes = [IsAdminUser]


    def simple(self,request):
        spus = SPU.objects.all()
        ser = SPUSerializer(spus,many=True)
        return Response(ser.data)