from rest_framework.generics import ListAPIView
from rest_framework.permissions import IsAdminUser
from rest_framework.viewsets import ModelViewSet
from rest_framework.response import Response
from django.contrib.auth.models import Permission,ContentType

from meiduo_admin.serializers.permission import PermissionSerializer, ContentTypeSerializer
from meiduo_admin.utils import PageNum
class Permissionview(ModelViewSet):
    # 指定序列化器
    serializer_class = PermissionSerializer
    # 指定查询集
    queryset = Permission.objects.all()
    # 分页
    pagination_class = PageNum
    # 认证
    permission_classes = [IsAdminUser]

    def content_types(self,request):

        data = ContentType.objects.all()

        res = ContentTypeSerializer(data,many=True)

        return Response(res.data)
