from rest_framework.generics import ListAPIView
from rest_framework.permissions import IsAdminUser
from rest_framework.viewsets import ModelViewSet
from rest_framework.response import Response

from goods.models import SPUSpecification, SPU,SpecificationOption
from meiduo_admin.serializers.simple import SpecificationOptionSerializer, SPUSpecificationSerializer
from meiduo_admin.utils import PageNum
class Simpleview(ModelViewSet):
    # 指定序列化器
    serializer_class = SpecificationOptionSerializer
    # 指定查询集
    queryset = SpecificationOption.objects.all()
    # 分页
    pagination_class = PageNum
    # 认证
    permission_classes = [IsAdminUser]


    def simple(self, request):

        data = SPUSpecification.objects.all()
        print(data)

        ser = SPUSpecificationSerializer(data, many=True)

        return Response(ser.data)
    #
# class OptionSimpleView(ModelViewSet):
#     serializer_class = SPUSpecificationSerializer
#     queryset = SPUSpecification.objects.all()