from rest_framework.permissions import IsAdminUser
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet
from django.contrib.auth.models import Group

from meiduo_admin.serializers.admins import UserSerializer, GroupSerializer
from users.models import User


from meiduo_admin.utils import PageNum
# 品牌的管理
class AdminsView(ModelViewSet):
    # 指定序列化器
    serializer_class = UserSerializer
    # 指定查询集
    queryset = User.objects.filter(is_staff=True)
    pagination_class = PageNum
    # 认证
    permission_classes = [IsAdminUser]

    def simple(self,request):
        data = Group.objects.all()

        res = GroupSerializer(data, many=True)

        return Response(res.data)
