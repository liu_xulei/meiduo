from rest_framework.generics import ListAPIView
from rest_framework.permissions import IsAdminUser
from rest_framework.viewsets import ModelViewSet
from rest_framework.response import Response
from django.contrib.auth.models import Permission,Group

from meiduo_admin.serializers.groups import GroupSerializer, PermissionSerializer
from meiduo_admin.utils import PageNum
class Groupview(ModelViewSet):
    # 指定序列化器
    serializer_class = GroupSerializer
    # 指定查询集
    queryset = Group.objects.all()
    # 分页
    pagination_class = PageNum
    # 认证
    permission_classes = [IsAdminUser]


    def simple(self,request):

        data = Permission.objects.all()

        res = PermissionSerializer(data,many=True)

        return Response(res.data)