from rest_framework.permissions import IsAdminUser
from rest_framework.viewsets import ModelViewSet
from rest_framework.response import Response
from goods.models import GoodsChannel, GoodsCategory,GoodsChannelGroup
from meiduo_admin.serializers.channels import GoodsChannelSerializer, GoodsChannelGroupSerializer
from meiduo_admin.serializers.skus import GoodsCategorySerializer

from meiduo_admin.utils import PageNum
# 频道的管理
class ChannlesView(ModelViewSet):
    # 指定序列化器
    serializer_class = GoodsChannelSerializer
    # 指定查询集
    queryset = GoodsChannel.objects.all()
    pagination_class = PageNum
    # 认证
    permission_classes = [IsAdminUser]

    def categories(self, request):
        # 获取一级分类信息
        data = GoodsCategory.objects.filter(parent=None)

        ser = GoodsCategorySerializer(data, many=True)

        return Response(ser.data)

    def channel_types(self,request):
        data = GoodsChannelGroup.objects.all()

        ser = GoodsChannelGroupSerializer(data,many=True)
        return Response(ser.data)
