from rest_framework.permissions import IsAdminUser
from rest_framework.viewsets import ModelViewSet
from rest_framework.response import Response
from goods.models import SKUImage, SKU
from meiduo_admin.serializers.image import SKUImageSerializer, SKUSerializer

from meiduo_admin.utils import PageNum
class Imageview(ModelViewSet):
    # 指定序列化器
    serializer_class = SKUImageSerializer
    # 指定查询集
    queryset = SKUImage.objects.all()
    pagination_class = PageNum
    # 认证
    permission_classes = [IsAdminUser]


    def simple(self,requset):
        spus = SKU.objects.all()
        ser = SKUSerializer(spus, many=True)
        return Response(ser.data)


