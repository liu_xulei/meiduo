from rest_framework.permissions import IsAdminUser
from rest_framework.viewsets import ModelViewSet
from rest_framework.response import Response
from goods.models import Brand
from meiduo_admin.serializers.brands import BrandSerializer
from meiduo_admin.serializers.channels import GoodsChannelSerializer

from meiduo_admin.utils import PageNum
# 品牌的管理
class BrandView(ModelViewSet):
    # 指定序列化器
    serializer_class = BrandSerializer
    # 指定查询集
    queryset = Brand.objects.all()
    pagination_class = PageNum
    # 认证
    permission_classes = [IsAdminUser]


