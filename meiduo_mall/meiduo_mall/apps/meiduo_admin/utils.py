from collections import OrderedDict

from rest_framework.pagination import PageNumberPagination
from rest_framework.response import Response


def  jwt_response_payload_handler(token, user, request):

    return {
        'username':user.username,
        'id':user.id,
        'token':token
    }


class PageNum(PageNumberPagination):

    page_size_query_param = 'pagesize'
    page_size = 5
    max_page_size = 10

    def get_paginated_response(self, data):
        return Response(OrderedDict([
            ('counts', self.page.paginator.count),
            ('page',self.page.number ),
            ('pages', self.page.paginator.num_pages),
            ('lists', data),
            ('pagesize',self.max_page_size),
        ]))
