from rest_framework import serializers

from goods.models import GoodsChannel, GoodsChannelGroup


class GoodsChannelSerializer(serializers.ModelSerializer):
    category = serializers.StringRelatedField(read_only=True)
    group = serializers.StringRelatedField(read_only=True)
    category_id = serializers.IntegerField()
    group_id = serializers.IntegerField()
    class Meta:
        model = GoodsChannel
        fields = '__all__'

class GoodsChannelGroupSerializer(serializers.ModelSerializer):

    class Meta:
        model = GoodsChannelGroup
        fields = '__all__'