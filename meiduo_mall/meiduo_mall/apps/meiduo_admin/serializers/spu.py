from rest_framework import serializers
from goods.models import SPU,Brand

class Brandserializers(serializers.ModelSerializer):

    class Meta:
        model = Brand
        fields = '__all__'




class Spuserializers(serializers.ModelSerializer):
    category1_id=serializers.IntegerField()
    category2_id=serializers.IntegerField()
    category3_id=serializers.IntegerField()
    brand_id = serializers.IntegerField()
    brand = serializers.StringRelatedField(read_only=True)
    category1=serializers.StringRelatedField(read_only=True)
    category2=serializers.StringRelatedField(read_only=True)
    category3=serializers.StringRelatedField(read_only=True)

    class Meta:
        model = SPU
        fields = '__all__'
