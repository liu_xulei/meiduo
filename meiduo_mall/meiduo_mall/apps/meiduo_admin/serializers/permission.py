from django.conf import settings
from fdfs_client.client import Fdfs_client, os
from rest_framework import serializers
from goods.models import SPUSpecification,SPU,SKUImage, SKU
from django.contrib.auth.models import Permission ,ContentType

class PermissionSerializer(serializers.ModelSerializer):


    class Meta:
        model = Permission
        fields= '__all__'


class ContentTypeSerializer(serializers.ModelSerializer):

    class Meta:
        model = ContentType
        fields = ('id','name')