from django.conf import settings
from fdfs_client.client import Fdfs_client, os
from rest_framework import serializers
from goods.models import SPUSpecification, SPU, SKUImage, SKU
from django.contrib.auth.models import Permission, Group


class GroupSerializer(serializers.ModelSerializer):
    class Meta:
        model = Group
        fields = '__all__'

class PermissionSerializer(serializers.ModelSerializer):

    class Meta:
        model = Permission
        fields = '__all__'