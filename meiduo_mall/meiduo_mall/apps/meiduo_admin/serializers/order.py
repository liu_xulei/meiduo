from rest_framework import serializers
from goods.models import SKU
from order.models import OrderInfo,OrderGoods

class SKUSerializer(serializers.ModelSerializer):

    class Meta:
        model = SKU
        fields = ('default_image','name')


class OrderGoodsSerializer(serializers.ModelSerializer):
    sku = SKUSerializer()
    class Meta:
        model = OrderGoods
        fields = ('sku','price','count')

class OrderInfoSerializer(serializers.ModelSerializer):

    skus = OrderGoodsSerializer(many=True)

    class Meta:

        model = OrderInfo
        fields = '__all__'
