from rest_framework import serializers
from goods.models import SPUSpecification,SPU
class SPUSpecificationSerializer(serializers.ModelSerializer):

    spu = serializers.StringRelatedField()
    spu_id = serializers.IntegerField()

    class Meta:
        model = SPUSpecification
        fields= ('name','id','spu','spu_id')

class SPUSerializer(serializers.ModelSerializer):

    class Meta:
        model = SPU
        fields = ('id','name')