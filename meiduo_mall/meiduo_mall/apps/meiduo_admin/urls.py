from django.conf.urls import url
from rest_framework.routers import DefaultRouter
from rest_framework_jwt.views import obtain_jwt_token

from meiduo_admin.views import views, users, specs, image, skus, order, spu, simple,channels,perminssion,groups,brands,admins

urlpatterns = [
    url(r'^authorizations/$', obtain_jwt_token),
    # 用户量统计
    url(r'^statistical/total_count/$', views.UserTotalCountView.as_view()),
    # 日增用户统计
    url(r'^statistical/day_increment/$', views.UserDayCountView.as_view()),
    # 日活跃用户
    url(r'^statistical/day_active/$', views.UserDayActiveCountView.as_view()),
    # 日下单用户
    url(r'^statistical/day_orders/$', views.UserDayOrdersView.as_view()),
    # 跃增用户
    url(r'^statistical/month_increment/$', views.UserMonthIncrementView.as_view()),
    # 日分类商品访问量
    url(r'^statistical/goods_day_views/$', views.UserGoodsDayView.as_view()),

    # 用户统计
    url(r'^users/$', users.Users.as_view()),
    # # 商品规格统计
    url(r'^goods/simple/$', specs.Specsview.as_view({'get': 'simple'})),
    # 图片新增sku
    url(r'^skus/simple/$', image.Imageview.as_view({'get': 'simple'})),
    # 图片新增sku
    url(r'^goods/(?P<pk>\d+)/specs/$', skus.SkusView.as_view({'get': 'simple'})),

    # SUP分类信息  simple

    url(r'^goods/brands/simple/$', spu.SpuView.as_view({'get': 'simple'})),
    url(r'^goods/channel/categories/$', spu.SpuView.as_view({'get': 'categories'})),
    url(r'^goods/channel/categories/(?P<pk>\d+)/$', spu.SpuView.as_view({'get': 'categoriesnum'})),

    # # 规格选型表goods/specs/simple/
    url(r'^goods/specs/simple/$', simple.Simpleview.as_view({'get':'simple'})),


# # 获取权限信息
    url(r'^permission/content_types/$', perminssion.Permissionview.as_view({'get':'content_types'})),

    url(r'^permission/simple/$', groups.Groupview.as_view({'get': 'simple'})),
    # 频道组表
    url(r'^goods/channel_types/$', channels.ChannlesView.as_view({'get': 'channel_types'})),
    # 分类信息的获取
    url(r'^goods/categories/$', channels.ChannlesView.as_view({'get': 'categories'})),

    # 组的获取
    url(r'^permission/groups/simple/$', admins.AdminsView.as_view({'get': 'simple'})),



]






# 规格
router = DefaultRouter()
router.register('goods/specs', specs.Specsview, base_name='specs')
urlpatterns += router.urls
# 图片
router = DefaultRouter()
router.register('skus/images', image.Imageview, base_name='image')
urlpatterns += router.urls

# SKU
router = DefaultRouter()
router.register('skus', skus.SkusView, base_name='skus')
urlpatterns += router.urls

# 订单表
router = DefaultRouter()
router.register('orders', order.Users, base_name='orders')
urlpatterns += router.urls


# 规格选项
router = DefaultRouter()
router.register('specs/options', simple.Simpleview, base_name='options')
urlpatterns += router.urls

# 规格选项获取
# router = DefaultRouter()
# router.register('goods/specs/simple', simple.OptionSimpleView, base_name='simple')
# urlpatterns += router.urls
#


# 频道表管理
router = DefaultRouter()
router.register('goods/channels', channels.ChannlesView, base_name='channels')
urlpatterns += router.urls

# 权限表管理
router = DefaultRouter()
router.register('permission/perms', perminssion.Permissionview, base_name='perms')
urlpatterns += router.urls

# 分组表管理
router = DefaultRouter()
router.register('permission/groups', groups.Groupview, base_name='groups')
router.register('goods/brands', brands.BrandView, base_name='brands')
urlpatterns += router.urls

# 管理员管理
router = DefaultRouter()
router.register('permission/admins', admins.AdminsView, base_name='admins')
urlpatterns += router.urls


# Spu
router = DefaultRouter()
router.register('goods', spu.SpuView, base_name='goods')
urlpatterns += router.urls
