from django.conf.urls import url
from django.contrib import admin
from . import views
urlpatterns = [
    url(r'^orders/settlement/$', views.OrdersSettlementView.as_view()),
    url(r'^orders/commit/$', views.OrdersCommitView.as_view()),
    # 提交订单成功
    url(r'^orders/success/$', views.OrdersSuccessView.as_view()),
    # 用户中心订单页面
    url(r'^orders/info/(?P<pk>\d+)/$', views.OrdersInfoView.as_view()),
    # 评价页面的展示
    url(r'^orders/comment/$', views.OrdersCommentView.as_view()),



]
